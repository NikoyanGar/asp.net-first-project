﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project2.Models
{
    public class Prompt
    {
        public int Id { get; set; }
        public string Headline { get; set; }
        public bool Status { get; set; }
    }
}
