﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Project2.Context;
using Project2.Models;

namespace Project2.Controllers
{

    public class HomeController : Controller
    {
        PromptContext db;
        public HomeController(PromptContext context)
        {
            db = context;
        }
        public IActionResult Index()
        {
            return View(db.Prompts.ToList());
        }
        public IActionResult Edit(int id)
        {

            var prompt = db.Prompts.FirstOrDefault(i => i.Id == id);
            prompt.Status = !prompt.Status;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public IActionResult Add(string heading)
        {

            db.Prompts.Add(new Prompt() { Headline = heading, Status = false });
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public IActionResult Remove(int id)
        {
            var x = db.Prompts.FirstOrDefault(i => i.Id == id);
            db.Prompts.Remove(x);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
