﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Project2.Migrations
{
    public partial class Inital : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Prompts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Headline = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prompts", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Prompts",
                columns: new[] { "Id", "Headline", "Status" },
                values: new object[] { 1, "Stay Home", true });

            migrationBuilder.InsertData(
                table: "Prompts",
                columns: new[] { "Id", "Headline", "Status" },
                values: new object[] { 2, "Dont Smoke", false });

            migrationBuilder.InsertData(
                table: "Prompts",
                columns: new[] { "Id", "Headline", "Status" },
                values: new object[] { 3, "Learn to program", true });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Prompts");
        }
    }
}
