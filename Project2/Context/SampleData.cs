﻿using Project2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project2.Context
{
    public static class SampleData
    {
        public static void Initialize(PromptContext context)
        {
            if (!context.Prompts.Any())
            {
                context.Prompts.AddRange(
                    new Prompt
                    {
                        Headline = "Stay Home",
                        Status = true
                    },
                    new Prompt
                    {
                        Headline = "Dont Smoke",
                        Status = false
                    },
                    new Prompt
                    {
                        Headline = "Learn to program",
                        Status = true
                    }
                ) ;
                context.SaveChanges();
            }
        }
    }
}
