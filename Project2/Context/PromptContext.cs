﻿using Microsoft.EntityFrameworkCore;
using Project2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project2.Context
{
    public class PromptContext:DbContext
    {
        public DbSet<Prompt> Prompts { get; set; }
        public PromptContext(DbContextOptions<PromptContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Prompt>().HasData(
                            new Prompt
                            {
                                Id = 1,
                                Headline = "Stay Home",
                                Status = true
                            },
                            new Prompt
                            {
                                Id = 2,
                                Headline = "Dont Smoke",
                                Status = false
                            },
                            new Prompt
                            {
                                Id = 3,
                                Headline = "Learn to program",
                                Status = true
                            }
                        );
        }
    }
}
